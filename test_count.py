import pytest
from pytest import raises
from unittest.mock import MagicMock
from digit_count import *

class Testclass:
    @pytest.fixture()
    def digit_count(self):
        digit_count=DigitCounter()
        return digit_count
        
        
    @classmethod
    def setup_class(cls):
        print("\n\nStart of the count\n")
        
        
    @classmethod
    def teardown_class(cls):
        print("\nEnd of the count\n\n")
        

    def test_count(self,digit_count):
        demo=47895
        digit_count.count_digit(demo)
        assert digit_count.count_digit(demo)==5
        

    @pytest.mark.parametrize("int, result", [(358976, 6), (8056, 4)])
    def test_count_multiple_digits(self, digit_count, int, result):
        a = digit_count.count_digit(int)
        assert a == result
    

    def test_Exception_with_bad_int(self,digit_count):
        with pytest.raises(Exception):
            digit_count.count_digit('')


    def test_return(self, digit_count, monkeypatch):
        mock_file = MagicMock()
        mock_file.readline = MagicMock(return_value=569834)
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        result = digit_count.read_from_file("user_input")
        mock_open.assert_called_once_with("user_input", "r")
        assert digit_count.count_digit(result)== 6
