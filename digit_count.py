class DigitCounter:
  def count_digit(self,n):
     if type(n)==int:
       n=str(n)
       return len(n)
     else:
         raise Exception("integer value input only")

  def read_from_file(self, filename):
     infile = open(filename, "r")
     line = infile.readline()
     return line

c=DigitCounter()
n=98756
print(f"Number of digits: {c.count_digit(n)}")


